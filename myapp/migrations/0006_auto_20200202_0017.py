# Generated by Django 3.0 on 2020-02-01 18:47

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('myapp', '0005_remove_post_author'),
    ]

    operations = [
        migrations.RenameField(
            model_name='post',
            old_name='body',
            new_name='schedule',
        ),
        migrations.RemoveField(
            model_name='post',
            name='title',
        ),
    ]
