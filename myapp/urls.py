from django.urls import path
from .views import BlogView, BlogsDetail, BlogCreate, BlogUpdate, BlogDelete

urlpatterns = [
    path('post/<int:pk>/delete/', BlogDelete.as_view(), name = 'post_delete'),
    path('post/new/', BlogCreate.as_view(), name = 'post_new'), 
    path('post/<int:pk>', BlogsDetail.as_view(), name = 'post_detail'),
    path('', BlogView.as_view(), name = 'home'),
    path('post/<int:pk>/edit/', BlogUpdate.as_view(), name = 'post_edit'),
]