
from django.shortcuts import render
from .models import Post
from django.urls import reverse_lazy

from django.views.generic.edit import (
    CreateView,   # helps the user to create or add a new post
    UpdateView,
    DeleteView,
)

from django.views.generic import (
    ListView,
    DetailView,
)


class BlogView(ListView):
    model         = Post
    template_name = 'home.html'


class BlogsDetail(DetailView):
    model         = Post
    template_name = 'post_detail.html'


class BlogCreate(CreateView):
    model = Post
    template_name = 'post_new.html'
    fields = '__all__'


class BlogUpdate(UpdateView):
    model         = Post
    template_name = 'post_edit.html'
    fields        = ['schedule']


class BlogDelete(DeleteView):
    model         = Post
    template_name = 'post_delete.html'
    success_url   = reverse_lazy('home')